package application;

import java.util.ArrayList;
import java.util.Collections;

import javafx.scene.image.Image;

public class LoteriaDeck{
	
	public ArrayList<Image> cards = new ArrayList<>();
	private int currentCard;
	private int time;
	
	public LoteriaDeck() {
		for(int i = 0 ; i < 55; i++) {
			cards.add(new Image("/Image/" + (i+1) + ".png"));
		}
		Collections.shuffle(cards);
	}
	
	public int getCurrentCard() {
		System.out.println("Current card:" + currentCard);
		return ++currentCard;
		
	}
	
	public int getTime() {
		return time;
	}
	
	public int getCurrentCardCount() {
		return this.currentCard;
	}


	public ArrayList<Image> getCards() {
		return cards;
	}

	public void setCards(ArrayList<Image> cards) {
		this.cards = cards;
	}


	
}
