package application;

import javafx.scene.image.Image;

public class LoteriaCards 
{
	private Image image;
	
	
	public LoteriaCards(Image image) {
		super();
		this.image = image;
	}
	
	public Image getImage() {
		return image;
	}
	public void setImage(Image image) {
		this.image = image;
	}
}
