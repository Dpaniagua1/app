package application;

import java.io.IOException;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Duration;

public class LoteriaController extends Main {
	
	public LoteriaDeck loteriaDeck;
	
	private Timeline timeLine;

    @FXML
    private Button startButton;

    @FXML
    private Button pauseButton;

    @FXML
    private Button settingsButton;
    
    @FXML
    private Button cardsCalledButton;

    @FXML
    private ImageView imageView;
    
    @FXML
    void handleCardsCalledButton(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(getClass().getResource("CardsCalled.fxml"));
    	Parent parent = loader.load();
    	Scene scene = new Scene(parent);
    	Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    	window.setScene(scene);
    	window.show();
    }

    @FXML
    void handlePauseButton(ActionEvent event) {
    	timeLine.stop();
    }

    @FXML
    void handleSettingsButton(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(getClass().getResource("Settings.fxml"));
    	Parent parent = loader.load();
    	Scene scene = new Scene(parent);
    	Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    	window.setScene(scene);
    	window.show();
    }

    @FXML
    void handleStartButton(ActionEvent event) {
    	timeLine.play();
    }
    

	public void initilize(){
    	loteriaDeck = new LoteriaDeck();
    	timeLine = new Timeline(
    			new KeyFrame(Duration.seconds(10)
    						, e -> {
    				imageView.setImage(loteriaDeck.getCards().get(loteriaDeck.getCurrentCard()));
    			}));
    	timeLine.setCycleCount(Animation.INDEFINITE);
    			
    }    




}
