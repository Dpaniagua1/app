package application;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;

public class SettingsController {

    @FXML
    private Button mainScreenButton;
    
    @FXML
    private Slider timeSlider;

    @FXML
    private Label timeLabel;

    @FXML
    void handleMaineScreenButton(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(getClass().getResource("Loteria.fxml"));
    	Parent parent = loader.load();
    	Scene scene = new Scene(parent);
    	Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    	window.setScene(scene);
    	window.show();
    }
    
    public void initialize() {
    	timeLabel.textProperty().bind(timeSlider.valueProperty().asString());
    }

}
