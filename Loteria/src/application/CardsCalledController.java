package application;

import java.io.IOException;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.image.ImageView;


public class CardsCalledController extends Main {
	

    @FXML
    private Button mainScreenButton;

    @FXML
    private ListView<ImageView> cardsCalledList;

    @FXML
    private Button viewCards;

   @FXML
   void handleMaineScreenButton(ActionEvent event) throws IOException {
	   FXMLLoader loader = new FXMLLoader();
	   loader.setLocation(getClass().getResource("Loteria.fxml"));
	   Parent parent = loader.load();
	   Scene scene = new Scene(parent);
	   Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
	   window.setScene(scene);
	   window.show();
}

    @FXML
    void handleViewCardsButton(ActionEvent event) {

    }
    
    public static void load() {
    	
    }

}

